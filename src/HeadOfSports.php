<?php
/**
* We use here a old school include style for loading classes
*/
include 'Coach.php';

class HeadOfSports {

  public $name;

  public function __construct($name = '') {
      $this->name = $name;
  }

  public function startTraining() {
    $coach = new Coach('John Doe');
    $coach->startTraining();
  }

}
