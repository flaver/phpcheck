<?php
/**
* We use here a old school include style for loading classes
*/
include 'Player.php';

class Coach {

  public $name;
  private $players;

  public function __construct($name='') {
    $this->name = $name;

    //Add players to the coach
    for ($i=0; $i < 12; $i++) {
      $this->addPlayer(new Player('John Doe'));
    }

  }

  public function startTraining() {
    echo "Coach ".$this->name.' starts training <br />';
    foreach ($this->players as $player) {
      $player->run();
      $player->doPushUps(30);
    }
    echo "Coach ".$this->name.' ends training <br />';
  }

  public function addPlayer(Player $player) {
    $this->players[] = $player;
  }

}
