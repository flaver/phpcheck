<?php
/**
 * EURO-NOTEN
 *
 * Auf jeder Euro-Banknote ist eine eindeutige Seriennummer aufgedruckt. Diese Seriennummer besteht aus einer
 * Länderkennung (L), zehn Ziffern (Z) und einer Prüfziffer (P). Die Struktur kann folglich mit dieser Symbolfolge
 * abstrahiert werden:
 *
 *      LZZZZZZZZZZP
 *
 * Die Länderkennung ist ein einzelner Buchstabe, welcher jeweils für ein Euro-Land steht. Beispielsweise X für
 * Deutschland, N für Österreich, P für die Niederlande oder Z für Belgien.
 *
 * Der Algorithmus zur Berechnung der Prüfziffer (P) lautet folgendermassen:
 *
 *  1) Interpretiere den Ländercode (L) als Zahl entsprechend der Position im Alphabet (A=1, B=2, C=3, ...)
 *  2) Bilde die Quersumme (Q) aus den ersten 11 bzw. 12 Ziffern
 *  3) Berechne den Rest der Ganzzahldivision (R) dieser Quersumme (Q) durch 9
 *  4) Berechne die Differenz (D) von 8 und dem oben berechneten Rest der Ganzzahldivision (R): D = 8 - R
 *  5) Unterscheide die beiden Fälle a) D = 0 und b) D ≠ 0
 *      a) Falls D = 0: Prüfziffer P = 9
 *      b) Falls D ≠ 0: Prüfziffer P = 8 - R
 *
 * Erstelle ein Programm, welches eine Euro-Note anhand der Seriennummer prüft. Dazu soll die Prüfziffer berechnet und
 * mit der gegebenen Prüfziffer verglichen werden. Gib eine entsprechende Meldung auf dem Bildschirm aus, ob die
 * Banknote echt oder gefälscht ist.
 */

if (isset($_GET['serialnumber'])) {

    $serialNumber = $_GET['serialnumber'];
    $serialArr = array();
    $inputP = 0;
    $calcQ = 0;
    $calcP = 0;
    $R = 0;
    $D = 0;
    $euroSerieTwo = false;

    $serialArr = str_split($serialNumber);

    //Counvert to binary int
    $inputP = intval($serialArr[11]);

    //Step 1+2
    for ($i=0; $i < 11; $i++) {

      switch ($i) {

        case 0:
          /**
          * "A" is in Assci 65 would leed to $L = 0 so we substract 64, onyl valid for uppercase....
          */
          $pos = ord($serialArr[$i]) - 64;
          if($pos > 9) {
            $z1 = intval(($pos / 10));
            $z2 = intval(($pos % 10));
            $calcQ = $z1 + $z2;
          } else {
            $calcQ = $pos;
          }

          break;

        case 1:
          /**
          *It can happen that the second one is also a letter(2013 Serie)
          */
          if(!is_numeric($serialArr[$i])) {
              $pos = ord($serialArr[$i]) - 64;

              if($pos > 9) {
                $z1 = intval(($pos / 10));
                $z2 = intval(($pos % 10));
                $calcQ += $z1 + $z2;
              } else {
                $calcQ += $pos;
              }

              $euroSerieTwo = true;

          } else {
            $calcQ += intval($serialArr[$i]);
          }

          break;

        default:
            $calcQ += intval($serialArr[$i]);
          break;
      }

    }

    //Step 3 + 4
    $R = $calcQ % 9;
    if($euroSerieTwo) {
      $D = 7 - $R;
    } else {
      $D = 8 - $R;
    }


    //Step 5
    if($D == 0) {
      $calcP = 9;
    } else {
      $calcP = $D;
    }

    //Check if we have "true" Money!
    if($calcP == $inputP) {
      $validationMessage = "Note ist okay!";
    } else {
      $validationMessage = "Note ist nicht okay!";
    }

}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>PhpCheck: Euro-Noten</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>

        <div id="content">
            <h1>Euro-Noten</h1>
            <p>Mit dem nachfolgenden Formular kann die Echtheit von Euro-Noten anhand der Seriennummer geprüft
                werden.</p>

            <form action="euroBills.php" method="GET" class="form center-form">
                <?php if (isset($serialNumber)): ?>
                    <input type="text" name="serialnumber" title="serialnumber"
                           value="<?php echo $serialNumber ?>"
                           class="input input-monospaced input-center"/>
                <?php else: ?>
                    <input type="text" name="serialnumber" title="serialnumber"
                           class="input input-monospaced input-center"/>
                <?php endif; ?>
                <br/>
                <input type="submit" value="Seriennummer prüfen"/>
            </form>

            <?php if (isset($validationMessage)): ?>
                <p class="validation-message"><?php echo $validationMessage ?></p>
            <?php endif; ?>
        </div>

    </body>
</html>
