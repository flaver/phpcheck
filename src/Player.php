<?php

class Player {

  public $name;

  public function __construct($name) {
    $this->name = $name;
  }

  public function run() {
    echo 'Player '.$this->name.' im running! <br />';
  }

  public function doPushUps(int $count) {
    echo 'Player '.$this->name." starting push ups <br />";
    for ($i=1; $i < $count+1; $i++) {
      echo $i."<br />";
    }
    echo 'Player '.$this->name." done with training! <br />";
  }

}
